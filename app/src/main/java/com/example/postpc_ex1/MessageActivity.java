package com.example.postpc_ex1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MessageActivity extends AppCompatActivity {
    AppDataManager manger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        manger = AppDataManager.getInstance(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        TextView tipe = (TextView) findViewById(R.id.tipeTextView);
        TextView manifacturerar = (TextView) findViewById(R.id.manifacturerPhoneText);
        TextView messageText = (TextView) findViewById(R.id.messageTextTextView);
        TextView timeText = (TextView) findViewById(R.id.whenText);
        final Message message =  getIntent().getParcelableExtra("MESSAGE");
        tipe.setText("Model: "+message.getModel());
        manifacturerar.setText("Manifacturer: "+message.getManufacturer());
        messageText.setText("Text: "+message.text);
        timeText.setText("Time: "+message.getDate());

        Button deletButtin = (Button) findViewById(R.id.deletButtin);
        deletButtin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manger.deletFromDB(message);
                Intent intent = new Intent(MessageActivity.this, MainActivity.class);
                MessageActivity.this.finish();
                startActivity(intent);
            }
        });
    }


}
