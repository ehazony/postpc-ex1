package com.example.postpc_ex1;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity
class Message implements Parcelable {


    @NonNull
    @PrimaryKey()
    public String uid;

    @ColumnInfo(name= "text")
    public String text;

    @ColumnInfo(name = "timestamp")
    private String timestamp;

    @ColumnInfo(name = "manufacturer")
    private String manufacturer;

    @ColumnInfo(name= "model")
    private String model;

    @ColumnInfo(name= "date")
    private String date;

    @Ignore
    Message(){}

    Message(String text) {
        this.text = text;
        Long tsLong = System.currentTimeMillis()/1000;
        this.timestamp = tsLong.toString();
        this.uid = UUID.randomUUID().toString();
        this.manufacturer = android.os.Build.MANUFACTURER;
        this.model = android.os.Build.MODEL;
        this.date = new Date().toString();
    }

    public Message(Parcel in) {
        this.text = in.readString();
        this.date= in.readString();
        this.manufacturer= in.readString();
        this.model= in.readString();
        this.timestamp= in.readString();
        this.uid = in.readString();
    }

//    public static Message makeMessage(String text){
//        return new Message("null",text);
//    }
    public static ArrayList<Message> getAll() {
        ArrayList<Message>a = new ArrayList<Message>();
//        a.add(new Message("df","test string"));
        return a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message person = (Message) o;
        return text.equals(person.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeString(this.date);
        dest.writeString(this.manufacturer);
        dest.writeString(this.model);
        dest.writeString(this.timestamp);
        dest.writeString(this.uid);

    }
    public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
