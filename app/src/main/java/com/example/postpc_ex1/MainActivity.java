package com.example.postpc_ex1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements MessageRecyclerUtils.MessageClickCallback {
    public static String USER_NAME= "USER_NAME";
    static final String TEXT_VIEW_HOLDER = "boxText";
    private static final String MESSAGES_HOLDER = "messages_holder";
    private static final String EMPTY_TEXT_MESSAGE = "you cant send an empty message";
    private String text;
    private  AppDataManager manger;

    private static MessageRecyclerUtils.MessageAdapter adapter = new MessageRecyclerUtils.MessageAdapter();

    private static AppRoomDatabase db;
//    private AppDataManager db;
    private ArrayList<Message> messages ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        this.manger= AppDataManager.getInstance(this);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        final EditText editText = (EditText) findViewById(R.id.editText);
        final RecyclerView recyclerView = findViewById(R.id.message_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String userName = sp.getString(USER_NAME, null);
        TextView textviue = (TextView) findViewById(R.id.textView);
        if(userName != null){
            textviue.setText("Hello "+userName);
        }
        else{
            textviue.setText("name not set");

        }
        db = AppRoomDatabase.getAppDatabase(this);
        messages = new ArrayList<>( db.MessageDao().getAll());

        Log.e("onCreate", "number of messages uploaded: "+ messages.size());

//        adapter.submitList( messages);
        adapter.callback = this;
        if (savedInstanceState != null) {
            editText.setText(savedInstanceState.getString(MESSAGES_HOLDER));
            adapter.submitList(messages);
        } else {
//            textview.setText("");
        }

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String text = editText.getText().toString();
                if (text.equals("")) {
                    Toast.makeText(getApplicationContext(), EMPTY_TEXT_MESSAGE, Toast.LENGTH_SHORT).show();
                    return;
                }
                Message m = new Message(text);

                ArrayList<Message> copyMessages = new ArrayList<>(messages);
//                db.MessageDao().insertAll(m);
                copyMessages.add(m);
                messages = copyMessages;
//                adapter.submitList(messages);
                manger.messageLiveData.postValue(messages);
                editText.setText("");
                manger.addToDb(m);
            }
        });

        final Observer<ArrayList<Message>> nameObserver = new Observer<ArrayList<Message>>() {
            @Override
            public void onChanged(ArrayList<Message> messages1) {
                messages = new ArrayList<>(messages1);
                adapter.submitList(messages);
            }

        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        manger.messageLiveData.observe(this, nameObserver);
    }



    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        final EditText editText = (EditText) findViewById(R.id.editText);
        savedInstanceState.putString(TEXT_VIEW_HOLDER, String.valueOf(editText.getText()));
        super.onSaveInstanceState(savedInstanceState);
        //saves to firebase on closing of the app
        manger.messageLiveData.setValue(messages);

// To restart user name:
//        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
//        editor.putBoolean("HAS_NAME", false);
//        editor.putString(USER_NAME, "test");
//        editor.commit();
    }

    @Override
    public void onMessageClick(final Message message) {
        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("MESSAGE", (Parcelable) message);
        startActivity(intent);
//        new AlertDialog.Builder(this)
//                .setTitle("Delete entry")
//                .setMessage("Are you sure you want to delete this entry?")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        ArrayList<Message> messagesCopy = new ArrayList<>(messages);
//                        messagesCopy.remove(message);
//                        messages = messagesCopy;
//                        adapter.submitList(messages);
//                        manger.messageLiveData.setValue(messages);
//                        manger.deletFromDB(message);
//                    }
//                })
//
//                // A null listener allows the button to dismiss the dialog and take no further action.
//                .setNegativeButton(android.R.string.no, null)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
    }



}







