package com.example.postpc_ex1;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MessageRecyclerUtils {

    static class  MessageCallback
    extends DiffUtil.ItemCallback<Message> {

        @Override
        public boolean areItemsTheSame(@NonNull Message message, @NonNull Message t1) {
            return message.text.equals(t1.text);
        }


        @Override
        public boolean areContentsTheSame(@NonNull Message message, @NonNull Message t1) {
            return message.equals(t1);
        }
    }

    interface MessageClickCallback {
        void onMessageClick(Message message);
    }

    static  class MessageAdapter
        extends ListAdapter<Message, MessageHolder>{



        public MessageAdapter(){
            super(new MessageCallback());
        }

        public MessageClickCallback callback;

        @NonNull @Override
        public MessageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            Context context =  viewGroup.getContext();
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_one_message,viewGroup,false);
            final MessageHolder holder = new MessageHolder(itemView);
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Message message = getItem(holder.getAdapterPosition());
                    if (callback != null){
                        callback.onMessageClick(message);
                        return true;
                }
                return false;
            }

            });
            return holder;


        }

        @Override
        public void onBindViewHolder(@NonNull MessageHolder messageHolder, int i) {
            Message message = getItem(i);
            messageHolder.textview.setText(message.text);
        }
    }



    private static class MessageHolder
    extends RecyclerView.ViewHolder {
        public final TextView textview;
        public MessageHolder(@NonNull View itemView) {
            super(itemView);
            textview = itemView.findViewById(R.id.message_text);
        }
    }


}
