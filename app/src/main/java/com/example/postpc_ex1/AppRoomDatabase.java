package com.example.postpc_ex1;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Message.class}, version =13 ,exportSchema = false)
public abstract class AppRoomDatabase extends RoomDatabase {
    private static AppRoomDatabase INSTACE ;

    public abstract MessageDao MessageDao();


    public static AppRoomDatabase getAppDatabase(Context context){
        if(INSTACE == null){
            INSTACE = Room.databaseBuilder(context.getApplicationContext(), AppRoomDatabase.class, "user-database").fallbackToDestructiveMigration()
                    .allowMainThreadQueries().build();
        }
        return INSTACE;
    }


}
