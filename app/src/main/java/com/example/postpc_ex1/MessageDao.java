package com.example.postpc_ex1;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


@Dao
public interface MessageDao{
    @Query("SELECT * FROM MESSAGE")
    List<Message> getAll();

    @Insert
    void insertAll(Message... messages);

    @Delete
    void delete(Message message);

    @Query("SELECT * FROM `Message` WHERE uid = :id LIMIT 1")
    List<Message> getItemId(String id);
}


