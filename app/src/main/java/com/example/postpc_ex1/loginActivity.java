package com.example.postpc_ex1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class loginActivity extends AppCompatActivity {
    private static final String USER_NAME = "USER_NAME";
    String HAS_NAME = "HAS_NAME";
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final EditText editText = (EditText) findViewById(R.id.editText);
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getBoolean(HAS_NAME, false)){
            Intent intent = new Intent(loginActivity.this, MainActivity.class);
            this.finish();
            startActivity(intent);
        }

        setContentView(R.layout.activity_login);
        final Button buttonName = (Button) findViewById(R.id.name);
        buttonName.setText("This is my name:");
        buttonName.setVisibility(View.GONE);
        buttonName.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // move to next screen pop activity from stack
                final EditText editText = (EditText) findViewById(R.id.editText);

                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                editor.putBoolean(HAS_NAME, true);
                String name = editText.getText().toString();
                editor.putString(USER_NAME, name);
                editor.apply();
                Intent intent = new Intent(loginActivity.this, MainActivity.class);
                AppDataManager db = AppDataManager.getInstance(getApplicationContext());
                db.addUserName(name);
                loginActivity.this.finish();
                startActivity(intent);

            }
        });
        @SuppressLint("CutPasteId") EditText editText1 = (EditText) findViewById(R.id.editText);
        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    buttonName.setVisibility(View.VISIBLE);
                } else {
                    buttonName.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });

        Button skip = (Button) findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(loginActivity.this, MainActivity.class);
                loginActivity.this.finish();

                startActivity(intent);
            }
        });

    }

}
