package com.example.postpc_ex1;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AppDataManager {

    private static AppDataManager Manger;
    private final FirebaseFirestore firestore;
    private CollectionReference mDatabaseRef;
    private AppRoomDatabase localDatabase;
    public MutableLiveData messageLiveData = new MutableLiveData<>();
    private Executor executor = Executors.newCachedThreadPool();
    public AppRoomDatabase roomDb;
    public String userName;
//    List messages;









    private AppDataManager(Context context) {
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        userName = sp.getString(MainActivity.USER_NAME, null);
        roomDb = AppRoomDatabase.getAppDatabase(context);
        messageLiveData.setValue(new ArrayList<Message>());
        firestore = FirebaseFirestore.getInstance();
        firestore.collection("defaults").document(userName).
                collection("texts").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                                //updates messages from firebase to lockal db
                                if (task.isSuccessful() && task.getResult() != null) {
                                    ArrayList<Message> allItemsFromFirestore = new ArrayList<>();
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        allItemsFromFirestore.add(document.toObject(Message.class));
                                    }
//                                    AppDataManager.this.messageLiveData.postValue(allItemsFromFirestore);

                                    for (Message m : roomDb.MessageDao().getAll()) {
                                        if(!allItemsFromFirestore.contains(m)){
                                            roomDb.MessageDao().delete(m);
                                        }
                                    }
                                    for (Message m : allItemsFromFirestore) {
                                        if (roomDb.MessageDao().getItemId((String.valueOf(m.uid))).size() == 0) {
                                            roomDb.MessageDao().insertAll(m);
                                        }
                                    }

                                    messageLiveData.setValue(roomDb.MessageDao().getAll());
                                }
                            }
                        });
    }

    public static AppDataManager getInstance(Context context)
    {
        if (Manger == null)
        {
            Manger = new AppDataManager(context);
        }
        return Manger;
    }


    public void addToDb(final Message m){
        executor.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        firestore.collection("defaults").document(userName).
                                collection("texts").add(m);
                        roomDb.MessageDao().insertAll(m);
                    }
                } );
    }

    public void deletFromDB(final Message m){
        executor.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        firestore.collection("defaults").document(userName).
                                collection("texts").whereEqualTo("uid", m.uid).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        firestore.collection("defaults").document(userName).
                                                collection("texts").document(document.getId())
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w(TAG, "Error deleting document", e);
                                                    }
                                                });
                                    }
                                }
                             else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            }
                        });
                        roomDb.MessageDao().delete(m);
                        messageLiveData.postValue(roomDb.MessageDao().getAll());
                    }
                } );
    }

    public void addUserName(final String name) {
        this.userName = name;
        executor.execute(
                new Runnable() {
                    @Override
                    public void run() {
//                        firestore.collection("defaults").add(name, {//
//                        });
                        Map<String, Object> data = new HashMap<>();

                        firestore.collection("defaults").document(name)
                                .set(data, SetOptions.merge());

                        for(Message m: roomDb.MessageDao().getAll()){
                            firestore.collection("defaults").document(userName).
                                    collection("texts").add(m);
                        }
                    }
                } );
    }
    public MutableLiveData getLiveData(){
        return this.messageLiveData;
    }

//
//    public void updateRemoteData() {
//        firestore.collection("texts").get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
//                        executor.execute(new Runnable() {
//                            @Override
//                            public void run() {
//                                List<Message> messages = messageLiveData.getValue();
//                                if (task.isSuccessful() && task.getResult() != null) {
//                                    ArrayList<Message> allItemsFromFirestore = new ArrayList<>();
//                                    for (QueryDocumentSnapshot document : task.getResult()) {
//                                        Message m = document.toObject(Message.class);
//                                        // removing message that was deleted in this session
//                                        if( !messages.contains(m)){
//                                            firestore.collection("texts").document(document.getId())
//                                                    .delete()
//                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
//                                                        @Override
//                                                        public void onSuccess(Void aVoid) {
//                                                            Log.d(TAG, "DocumentSnapshot successfully deleted!");
//                                                        }
//                                                    })
//                                                    .addOnFailureListener(new OnFailureListener() {
//                                                        @Override
//                                                        public void onFailure(@NonNull Exception e) {
//                                                            Log.w(TAG, "Error deleting document", e);
//                                                        }
//                                                    });
//                                        }
//                                        else{
//                                            allItemsFromFirestore.add(m);
//                                        }
//                                    }
//                                    // Adding messages that were added in this session
//                                    for (Message m: Objects.requireNonNull(messages)){
//                                        if(!allItemsFromFirestore.contains(m)){
//                                            firestore.collection("texts").add(m);
//                                            Log.d(TAG, "adding message to firebase with id: " + m.uid + "--> " + m.text);
//                                        }
//                                    }
//                                }
//                            }
//                        });
//                    }
//                });
//    }
}
